<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sendEmails', 'EmailActivationController@sendActivationEmail');
Route::get('/token/{token}', 'EmailActivationController@sendChangePasswordPage');

Route::post('/token/{token}', 'EmailActivationController@storeActivatedEmail');

Route::get('/testview', function () {
    return view('changePassword');
});
