<?php

/**
 * @author Jedidiah Anthony <jediidah.anthony@concept-nova.com>
 * 
 */

namespace App\Http\Controllers;

use App\Mail\SendActivationLink;
use App\Mail\SendActivationNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use PHPUnit\Framework\Constraint\IsEmpty;

class EmailActivationController extends Controller
{
    //
    /**
     * Sends email to unactivated users 
     * 
     * @return view
     */
    public function sendActivationEmail()
    {
        $startDate = date('2019-07-01');
        $endDate = date('2019-10-18');
        $users = User::where('status', 0)->whereBetween('created_at', [$startDate, $endDate])->get();
        // dd($users);
        foreach ($users as $user) {
            Mail::to($user)->send(new SendActivationLink($user->activation_code));
        }
        dd('Finish sending the emails');
    }

    /**
     * Searches if the token exists in the database
     * 
     * @return view
     */
    public function sendChangePasswordPage($token)
    {
        $user = User::where('activation_code', $token)->first();
        if ($user) {
            return view('changePassword', compact('user', 'token'));
        }
    }
    /**
     * Activate user account with the new password
     * 
     * @param Request $request 
     * 
     * @return view 
     */
    public function storeActivatedEmail(Request $request)
    {

        $request->validate([
            'password' => 'required|min:3',
            'confirmpassword' => 'required|min:3'
        ]);

        if ($request->input('password') !== $request->input('confirmpassword')) {

            return redirect()->back()->with('errors', 'An error occurs');
        }
        $token = $request->input('token');
        $user = User::where('activation_code', $token)->first();
        $user->activation_code = '';
        $user->password = \Hash::make($request['password']);
        $user->status = 1;
        $user->save();
        Mail::to($user)->send(new SendActivationNotification($user));
        return view('changePasswordSuccessful');
    }
}
